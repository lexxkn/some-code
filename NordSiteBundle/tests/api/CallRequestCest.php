<?php

namespace Nord\NordSiteBundle;

use Codeception\Util\HttpCode;

/**
 * @covers  \Nord\CallRequestBundle\CallRequestBundle
 *
 * @group nord
 * @group api
 * @group callRequest
 */
class CallRequestCest
{
    public function _before(ApiTester $I)
    {
        $I->haveHttpHeader('Accept', 'application/json');
        $I->haveHttpHeader('Content-Type', 'application/json');
    }

    /**
     * @param ApiTester $I
     *
     * @group negative
     */
    public function errorsExpectationTest(ApiTester $I)
    {
        $I->wantToTest('callRequest create api [negative]');
        $I->sendPOST(
            '/v1/callRequest',
            [
                'name'  => '',
                'phone' => '',
            ]
        );

        $I->seeResponseCodeIs(HttpCode::UNPROCESSABLE_ENTITY);
        $I->seeResponseIsJson();
        $I->seeResponseMatchesJsonType([
            'code'        => 'integer',
            'message'     => 'string',
            'errorFields' => [
                'phone' => 'string',
            ],
        ]);


        $I->sendPOST(
            '/v1/callRequest',
            [
                'name'  => 'Lorem Ipsum',
                'phone' => '1234565',
            ]
        );

        $I->seeResponseCodeIs(HttpCode::UNPROCESSABLE_ENTITY);
        $I->seeResponseIsJson();
        $I->seeResponseMatchesJsonType([
            'code'        => 'integer',
            'message'     => 'string',
            'errorFields' => [
                'phone' => 'string',
            ],
        ]);
    }

    /**
     * @param ApiTester $I
     *
     * @group positive 
     */
    public function successStoredTest(ApiTester $I)
    {
        $I->wantToTest('callRequest create api [positive]');
        $I->sendPOST(
            '/v1/callRequest',
            [
                'name'  => 'Дмитрий Пучков',
                'phone' => '+7 (900) 123-45-67',
            ]
        );

        $I->seeResponseCodeIs(HttpCode::OK);
        $I->seeResponseIsJson();
        $I->seeResponseMatchesJsonType([
            'success' => 'boolean',
            'message' => 'string',
        ]);

        $I->canSeeInDatabase('nord_call_request', [
            'name'           => 'Дмитрий Пучков',
            'phone'          => '+79001234567',
            'status'         => 'CREATED',
        ]);

    }

}
