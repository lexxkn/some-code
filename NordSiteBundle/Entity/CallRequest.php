<?php

namespace Nord\NordSiteBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Nord\CallRequestBundle\Entity\CallRequest as BaseCallRequest;

/**
 * CallRequest - заказанный обратный звонок.
 *
 * @ORM\Entity
 * @ORM\Table(name="nord_call_request")
 */
class CallRequest extends BaseCallRequest
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     *
     * @Serializer\Groups({"api"})
     */
    protected $id;
}
