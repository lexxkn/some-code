<?php

namespace Nord\CallRequestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Nord\CustomerBundle\Validator\Constraints\PhoneNumber as AssertPhoneNumber;
use Nord\STDLibBundle\Util\Canonicalizer\PhoneCanonicalizer;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * CallRequest entity.
 *
 * @ORM\MappedSuperclass
 * @ORM\HasLifecycleCallbacks
 */
class CallRequest implements CallRequestInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     *
     * @Serializer\SerializedName("id")
     * @Serializer\Type("integer")
     * @Serializer\Groups({"api"})
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     *
     * @Serializer\SerializedName("name")
     * @Serializer\Type("string")
     * @Serializer\Groups({"callRequest_api", "api"})
     */
    protected $name;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=12, nullable=false)
     *
     * @Serializer\SerializedName("phone")
     * @Serializer\Type("string")
     * @Serializer\Groups({"callRequest_api", "api"})
     *
     * @Assert\NotBlank(message="Укажите номер мобильного телефона")
     *
     * @AssertPhoneNumber(defaultRegion="RU", type="mobile", message="Введите корректный номер мобильного телефона")
     */
    protected $phone;

    /**
     * @var \DateTimeImmutable|null
     *
     * @ORM\Column(name="created", type="datetime_immutable", nullable=true)
     */
    protected $created;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return CallRequestInterface
     */
    public function setId(int $id): CallRequestInterface
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     *
     * @return CallRequestInterface
     */
    public function setName(?string $name): CallRequestInterface
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPhone(): ?string
    {
        return $this->phone;
    }

    /**
     * @param string|null $phone
     *
     * @return CallRequestInterface
     */
    public function setPhone(?string $phone): CallRequestInterface
    {
        $this->phone = PhoneCanonicalizer::canonize($phone);

        return $this;
    }

    /**
     * Gets triggered only on insert.
     *
     * @ORM\PrePersist
     *
     * @return CallRequestInterface
     */
    public function onCreated()
    {
        $this->created = new \DateTimeImmutable();

        return $this;
    }
}
