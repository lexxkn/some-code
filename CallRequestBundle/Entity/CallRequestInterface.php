<?php

namespace Nord\CallRequestBundle\Entity;

interface CallRequestInterface
{
    /**
     * @return int
     */
    public function getId(): int;

    /**
     * @param int $id
     *
     * @return CallRequestInterface
     */
    public function setId(int $id): self;

    /**
     * @return string
     */
    public function getName(): ?string;

    /**
     * @param string $name
     *
     * @return CallRequestInterface
     */
    public function setName(?string $name): self;

    /**
     * @return string
     */
    public function getPhone(): ?string;

    /**
     * @param string $phone
     *
     * @return CallRequestInterface
     */
    public function setPhone(?string $phone): self;
}
