<?php

namespace Nord\CallRequestBundle\Controller;

use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use JMS\Serializer\SerializerInterface;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nord\CallRequestBundle\Entity\CallRequestInterface;
use Nord\STDLibBundle\Exception\ApiException;
use Nord\STDLibBundle\Exception\ApiValidationException;
use Nord\STDLibBundle\Model\ApiSuccessModel;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class CallRequestController.
 *
 * @Rest\NamePrefix("api_callRequest")
 * @Rest\Prefix("callRequest")
 */
class CallRequestController
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * @var CallRequestInterface
     */
    private $callRequest;

    /**
     * @param EntityManagerInterface $em
     * @param SerializerInterface    $serializer
     * @param ValidatorInterface     $validator
     * @param CallRequestInterface   $callRequest
     */
    public function __construct(
        EntityManagerInterface $em,
        SerializerInterface $serializer,
        ValidatorInterface $validator,
        CallRequestInterface $callRequest
    ) {
        $this->em = $em;
        $this->serializer = $serializer;
        $this->validator = $validator;
        $this->callRequest = $callRequest;
    }

    /**
     * @Rest\Post(path="")
     *
     * @Rest\View(serializerGroups={"api_callRequest", "api"})
     *
     * @SWG\Post(
     *     summary="Call request",
     *     consumes={"application/json"},
     *     produces={"application/json"},
     *     tags={"СallRequest"},
     *     @SWG\Parameter(
     *         name="body",
     *         in="body",
     *         required=true,
     *         type="object",
     *         @Model(type=Nord\CallRequestBundle\Entity\CallRequest::class, groups={"callRequest_api"})
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Ok",
     *         @Model(type=Nord\STDLibBundle\Model\ApiSuccessModel::class, groups={"api"})
     *     ),
     *     @SWG\Response(
     *         response=404,
     *         description="No resource found",
     *         @Model(type=Nord\STDLibBundle\Exception\ApiException::class, groups={"api"})
     *     ),
     *     @SWG\Response(
     *         response=401,
     *         description="Unauthorized",
     *         @Model(type=Nord\STDLibBundle\Exception\ApiException::class, groups={"api"})
     *     ),
     *     @SWG\Response(
     *         response=500,
     *         description="Internal server error",
     *         @Model(type=Nord\STDLibBundle\Exception\ApiException::class, groups={"api"})
     *     )
     * )
     *
     * @param Request $request
     *
     * @throws \LogicException
     *
     * @return ApiSuccessModel
     */
    public function addAction(Request $request): ApiSuccessModel
    {
        $class = get_class($this->callRequest);

        /** @var CallRequestInterface $callRequest */
        $callRequest = new $class();

        $callRequest->setName($request->get('name'));
        $callRequest->setPhone($request->get('phone'));

        if (count($violations = $this->validator->validate($callRequest))) {
            throw new ApiValidationException($violations);
        }

        try {
            $this->em->persist($callRequest);
            $this->em->flush();
        } catch (\Throwable $throwable) {
            throw new ApiException(Response::HTTP_BAD_REQUEST, $throwable->getMessage(), $throwable);
        }

        return (new ApiSuccessModel())->setMessage('Success');
    }
}
